using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CubeOfDarkness
{
  public class Tile : MonoBehaviour
  {
    Renderer rend;
    Collider[] results = new Collider[5];

    Color original_color;

    const float color_change_speed = 4.0f;

    int mask;

    void Start()
    {
      mask = 1 << LayerMask.NameToLayer("player");
      rend = GetComponentInChildren<Renderer>();
      original_color = rend.material.color;
    }

    public Vector3 GetSnapPosition()
    {
      return new Vector3(
        Mathf.Round(transform.position.x),
        0.0f,
        Mathf.Round(transform.position.z)
      );
    }

    public void SetColor(Color target_color)
    {
      rend.material.color = target_color;
    }

    void LateUpdate()
    {
      if(rend.material.color != original_color)
        rend.material.color = Color.Lerp(rend.material.color, original_color, Time.deltaTime * color_change_speed);
    }
  }
}
