using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess(typeof(InvertColorsEffectRenderer), PostProcessEvent.AfterStack, "Custom/InvertColorsEffect")]
public sealed class InvertColorsEffect : PostProcessEffectSettings
{
    [Range(0f, 1f), Tooltip("InvertColorsEffect effect intensity.")]
    public FloatParameter intensity = new FloatParameter { value = 1.0f };
}

public sealed class InvertColorsEffectRenderer : PostProcessEffectRenderer<InvertColorsEffect>
{
    public override void Render(PostProcessRenderContext context)
    {
        var sheet = context.propertySheets.Get(Shader.Find("Hidden/InvertColors"));
        sheet.properties.SetFloat("_Intensity", settings.intensity);
        context.command.BlitFullscreenTriangle(context.source, context.destination, sheet, 0);
    }
}
