using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CubeOfDarkness {

  public class Box : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IBeginDragHandler, IEndDragHandler
  {
    [Range(2, 3)]
    public uint size = 2;

    public Color color = Color.blue;

    [System.NonSerialized]
    const float SNAP_SPEED = 2;

    Vector3 prevTouchPosition;

    IEnumerator snap;
    IEnumerator change_y_scale;

    public const float FULL_CHANGE_SCALE_SPEED = 0.25f;

    float y;

    static Plane XZPlane = new Plane(Vector3.up, Vector3.zero);

    Level level;

    public AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, 1, 1);

    public void OnValidate()
    {
      ApplySize();
    }

    Transform GetChild()
    {
      if(transform.childCount == 0)
      {
        var child = GameObject.CreatePrimitive(PrimitiveType.Cube);
        child.transform.SetParent(transform, false);
      }
      return transform.GetChild(0);
    }

    public void ApplySize(bool hide = false)
    {
      var child = GetChild();
      child.localScale = new Vector3(size, hide ? 0.01f : 1.0f, 1.0f);
      child.localPosition = new Vector3(0.5f * (size - 1), 0.5f, 0.0f);
    }

    void OnDrawGizmos()
    {
      var child = transform.GetChild(0);
      Gizmos.color = color;
      if(transform.right == Vector3.right)
        Gizmos.DrawCube(child.position, child.localScale);
      else
      {
        var size = new Vector3(child.localScale.z, child.localScale.y, child.localScale.x);
        Gizmos.DrawCube(child.position, size);
      }
    }

    static int color_random_index = 0;

    void DoInit(bool hide)
    {
      prevSnapPosition = GetSnapPosition();
      var child = GetChild();
      level = GetComponentInParent<Level>();

      if(hide)
        child.gameObject.SetActive(false);

      ApplySize(hide);

      if(color == Color.white)
      {
        var colors = new Color[] {Color.blue, Color.cyan, Color.gray, Color.green, Color.grey, Color.magenta, Color.yellow};
        color = colors[color_random_index++];
        color_random_index = color_random_index % colors.Length;
      }
      child.GetComponent<Renderer>().material.color = color;
    }

    public void Start()
    {
      if(start == null)
        DoInit(false);
      else
        start();
    }

    public static Vector3 GetMousePositionOnXZPlane(Vector2 position) {
      float distance;
      Ray ray = Camera.main.ScreenPointToRay(position);
      if(XZPlane.Raycast(ray, out distance))
      {
        Vector3 hitPoint = ray.GetPoint(distance);
        hitPoint.y = 0;
        return hitPoint;
      }
      return Vector3.zero;
    }

    public Vector3 Clamp(ref Vector3 value, ref Vector3 min, ref Vector3 max)
    {
      value.x = Mathf.Clamp(value.x, min.x, max.x);
      value.z = Mathf.Clamp(value.z, min.z, max.z);
      return value;
    }

    [System.NonSerialized]
    public Vector3 min = new Vector3(0, 0, 3);
    [System.NonSerialized]
    public Vector3 max = new Vector3(1, 0, 3);

    public void OnDrag(PointerEventData eventData)
    {
      var delta = GetMousePositionOnXZPlane(eventData.position) - prevTouchPosition;
      var position = transform.position;
      var move = Vector3.Scale(transform.right, delta);
      position += move;
      position.y = y;
      transform.position = Clamp(ref position, ref min, ref max);
      prevTouchPosition = GetMousePositionOnXZPlane(eventData.position);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
      prevTouchPosition = GetMousePositionOnXZPlane(eventData.position);
      if(snap != null)
        StopCoroutine(snap);
      GetMinMax();
    }

    void GetMinMax()
    {
      level.GetMinMax(this);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }

    public Vector3 GetSnapPosition()
    {
      snapPosition.x = Mathf.Round(transform.position.x);
      snapPosition.y = 0.0f;
      snapPosition.z = Mathf.Round(transform.position.z);
      return snapPosition;
    }

    IEnumerator Snap()
    {
      var target = new Vector3(
        Mathf.Round(transform.position.x),
        0.0f,
        Mathf.Round(transform.position.z)
      );
      while(target != transform.position)
      {
        var delta = (target - transform.position);
        var move = delta.normalized * Time.deltaTime * SNAP_SPEED;
        if(delta.magnitude < move.magnitude)
          move = delta;
        transform.position += move;
        yield return null;
      }
    }

    public void OnPointerDown(PointerEventData pointerEventData)
    {
      y = 0.10f;
      transform.Translate(0, y, 0);

      level.HideOther(this);
      level.StartMove(this);
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
      level.ShowAll();

      y = 0.0f;

      snap = Snap();
      StartCoroutine(snap);
      level.FinishMove(this);
    }

    Vector3 scale;
    Vector3 start_scale;
    Vector3 position;

    void UpdatePosition(Transform child)
    {
      position.y = child.localScale.y * 0.5f;
      child.localPosition = position;
    }

    IEnumerator DoChangeScaleY(float target_y, bool hide)
    {
      var child = GetChild();
      float full_time = Mathf.Abs(target_y - child.localScale.y) * FULL_CHANGE_SCALE_SPEED;;

      scale = child.localScale;
      start_scale = scale;
      scale.y = target_y;
      position = child.localPosition;

      child.gameObject.SetActive(true);

      yield return Tween.DoTween(full_time, (float t) => {
        child.localScale = Vector3.Lerp(start_scale, scale, curve.Evaluate(t));
        UpdatePosition(child);
      });

      child.gameObject.SetActive(!hide);

      change_y_scale = null;
    }

    public bool IsBusy()
    {
      return change_y_scale != null || GetChild().localScale.y < 1.0f || !GetChild().gameObject.activeSelf || !gameObject.activeSelf;
    }

    public void ChangeScaleY(float target_y, bool hide = false)
    {
      if(change_y_scale != null)
        StopCoroutine(change_y_scale);

      change_y_scale = DoChangeScaleY(target_y, hide);
      StartCoroutine(change_y_scale);
    }

    System.Action start = null;

    public void Init(Data.DataBox box, bool hide)
    {
      gameObject.SetActive(true);

      size = box.size;
      color = box.color;
      transform.position = new Vector3(box.x, 0, box.y);
      if(box.vertical)
        transform.right = Vector3.forward;
      else
        transform.right = Vector3.right;

      ApplySize(true);

      start = () => {
        DoInit(true);
        if(!hide)
          ChangeScaleY(1.0f);
        start = null;
      };
    }

    public Vector3 snapPosition;
    public Vector3 prevSnapPosition;

    void LateUpdate()
    {
      prevSnapPosition = GetSnapPosition();
    }
  }
}
