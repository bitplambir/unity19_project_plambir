using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;

namespace CubeOfDarkness
{
  [ExecuteAlways]
  public class Level : MonoBehaviour
  {
    const string TILE_NAME = "tile";
    const string PLANE_NAME = "plane";
    const string EXIT_NAME = "exit";

    public GameObject tile;
    public GameObject plane;
    public GameObject exit;
    public GameObject box;

    public uint width;
    public uint height;
    public uint exitPosition;

    public Vector3 center;

    public bool is_preview_mode;

    const uint EXIT_SIZE = 2;

    public List<GameObject> tiles = new List<GameObject>();
    public Dictionary<int, Tile> tile_map = new Dictionary<int, Tile>();

    public System.Action onComplete;
    bool haveDragBox;

    public Button undo;
    public Button redo;
    public Button levels;

    public AnimationCurve playMoveCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

    GameObject Get(string name, GameObject prefab)
    {
      GameObject finded = null;

      for(int i = 0; i < transform.childCount; ++i)
      {
        var go = transform.GetChild(i).gameObject;
        if(go.name != name)
          continue;

        finded = go;
        break;
      }

      if(finded == null)
      {
        finded = Instantiate(prefab, transform);
        finded.name = name;
      }

      return finded;
    }

    void PrepareExit()
    {
      if(exit == null)
        return;

      GameObject finded_exit = Get(EXIT_NAME, exit);
      finded_exit.transform.position = new Vector3(width, 0.0f, exitPosition);
    }

    void PrepareTile()
    {
      if(tile == null)
        return;

      tiles.Clear();

      for(int i = 0; i < transform.childCount; ++i)
      {
        var go = transform.GetChild(i).gameObject;
        if(go.name != TILE_NAME)
          continue;

        go.SetActive(tiles.Count < (width * height));
        tiles.Add(go);
      }

      for(int i = 0; i < width; ++i)
      {
        for(int j = 0; j < height; ++j)
        {
          int index = (int)(i * height + j);
          if(index == tiles.Count)
            tiles.Add(Instantiate(tile, transform));

          var t = tiles[index];
          t.name = TILE_NAME;
          t.transform.position = new Vector3(i, 0, j);

          var tile_co = t.GetComponent<Tile>();
          var pos = tile_co.GetSnapPosition();
          tile_map[Index(ref pos)] = tile_co;

          index++;
        }
      }

      center = new Vector3((width - 1) / 2.0f, 0.0f, (height - 1) / 2.0f);
    }

    void PreparePlane()
    {
      if(plane == null)
        return;

      GameObject finded_plane = Get(PLANE_NAME, plane);

      finded_plane.transform.position = center;
      finded_plane.transform.localScale = new Vector3(width + 0.1f, finded_plane.transform.localScale.y, height + 0.1f);
    }

    List<Box> boxes = new List<Box>();

    void MakeModel()
    {
      GetComponentsInChildren<Box>(true, boxes);
    }

    bool[,] map;

    void SetMap(Vector3 position, bool v)
    {
      var x = Mathf.RoundToInt(position.x);
      var y = Mathf.RoundToInt(position.z);

      if(x < 0)
        return;
      if(y < 0)
        return;
      if(x >= width + EXIT_SIZE)
        return;
      if(y >= height)
        return;

      map[x, y] = v;
    }

    bool GetMap(Vector3 position)
    {
      return map[(int)position.x, (int)position.z];
    }

    public void GetMinMax(Box target)
    {
      ((IList)map).Clear();

      foreach(var box in boxes)
      {
        if(!box.gameObject.activeSelf)
          continue;

        if(box.GetInstanceID() == target.GetInstanceID())
          continue;

        for(int i = 0; i < box.size; ++i)
          SetMap(box.GetSnapPosition() + box.transform.right * i, true);
      }

      bool vertical = target.transform.right == Vector3.right;

      target.min = target.transform.position;
      target.max = target.transform.position;

      float v = vertical ? target.GetSnapPosition().x : target.GetSnapPosition().z;
      for(var min = target.GetSnapPosition(); v >= 0; --v)
      {
        if(vertical)
          min.x = v;
        else
          min.z = v;

        if(!GetMap(min))
          target.min = min;
        else
          break;
      }

      v = vertical ? target.GetSnapPosition().x : target.GetSnapPosition().z;
      var size = (vertical ? width : height);
      if(vertical && target.GetSnapPosition().z == exitPosition)
        size += EXIT_SIZE;
      for(var max = target.GetSnapPosition(); v < size; ++v)
      {
        if(vertical)
          max.x = v;
        else
          max.z = v;

        if(!GetMap(max))
          target.max = max;
        else
          break;
      }

      var size_correction = target.size - 1;
      if(vertical)
        target.max.x = Mathf.Max(target.min.x, target.max.x - size_correction);
      else
        target.max.z = Mathf.Max(target.min.z, target.max.z - size_correction);
    }

    public void Start()
    {
      PrepareTile();
      PreparePlane();
      PrepareExit();
      MakeModel();
      map = new bool[width + EXIT_SIZE, height];

      undo.onClick.RemoveAllListeners();
      undo.onClick.AddListener(Undo);
      redo.onClick.RemoveAllListeners();
      redo.onClick.AddListener(Redo);
      levels.onClick.RemoveAllListeners();
      levels.onClick.AddListener(Levels);
    }

    void Levels()
    {
      Game.self.LevelSelect();
    }

    Vector3 hide_scale = new Vector3(1, 0, 1);

    public void Init(Data.DataLevel level, bool hide = false)
    {
      moves.Clear();
      current_move_count = 0;

      width = level.width;
      height = level.height;
      exitPosition = level.exitPosition;
      moves = level.moves;
      current_move_count = level.current_move_count;

      Start();

      foreach(var box in boxes)
        box.gameObject.SetActive(false);

      for(int i = 0; i < level.boxes.Count; ++i)
      {
        if(i >= boxes.Count)
        {
          var new_box = GameObject.Instantiate(this.box);
          new_box.transform.SetParent(transform);
          boxes.Add(new_box.GetComponent<Box>());
        }

        var box = boxes[i];
        box.Init(level.boxes[i], hide);
      }
    }

    public Data.DataLevel ToData()
    {
      var data_level = new Data.DataLevel();
      data_level.width = width;
      data_level.height = height;
      data_level.exitPosition = exitPosition;
      if(Application.isPlaying)
      {
        data_level.moves = moves;
        data_level.current_move_count = current_move_count;
      }

      for(int i = 0; i < transform.childCount; ++i)
      {
        var child = transform.GetChild(i);
        var box = child.gameObject.GetComponent<Box>();
        if(box == null || !box.gameObject.activeSelf)
          continue;

        var data_box = new Data.DataBox();
        data_box.x = Mathf.RoundToInt(box.GetSnapPosition().x);
        data_box.y = Mathf.RoundToInt(box.GetSnapPosition().z);
        data_box.vertical = box.transform.right != Vector3.right;
        data_box.color = box.GetComponent<Box>().color;
        data_box.size = box.GetComponent<Box>().size;
        data_level.boxes.Add(data_box);
      }

      return data_level;
    }

    public void HideOther(Box target, float scale_y = 0.6f)
    {
      haveDragBox = true;

      foreach(var box in boxes)
      {
        if(!box.gameObject.activeSelf)
          continue;

        if(box.GetInstanceID() == target.GetInstanceID())
          continue;

        box.ChangeScaleY(scale_y);
      }
    }

    public void HideAll(float scale_y = 0.01f)
    {
      haveDragBox = true;

      foreach(var box in boxes)
      {
        if(!box.gameObject.activeSelf)
          continue;

        box.ChangeScaleY(scale_y, hide: true);
      }
    }

    void Update()
    {
      UpdateTileColor();
      CheckMenu();
      CheckLevelComplete();
    }

    void UpdateTileColor()
    {
      if(!Application.isPlaying)
        return;

      foreach(var box in boxes)
      {
        if(box.IsBusy())
          continue;

        int max_steps = 6;
        for(var p = box.prevSnapPosition; p != box.GetSnapPosition(); p += (box.GetSnapPosition() - box.prevSnapPosition).normalized)
        {
          UpdateTileColorByPosition(p, box.color);

          if(max_steps-- < 0)
            break;
        }

        for(int i = 0; i < box.size; ++i)
          UpdateTileColorByPosition(box.prevSnapPosition + box.transform.right * i, box.color);

        for(int i = 0; i < box.size; ++i)
          UpdateTileColorByPosition(box.GetSnapPosition() + box.transform.right * i, box.color);
      }
    }

    void UpdateTileColorByPosition(Vector3 index, Color color)
    {
      if(tile_map.ContainsKey(Index(ref index)))
        tile_map[Index(ref index)].SetColor(color);
    }

    int Index(ref Vector3 snap_position)
    {
      return Mathf.RoundToInt(snap_position.x) * (int)height + Mathf.RoundToInt(snap_position.z);
    }

    void CheckMenu()
    {
      if(is_preview_mode)
        return;

      if(!Application.isPlaying)
        return;

      undo.gameObject.SetActive(current_move_count != 0);
      redo.gameObject.SetActive(current_move_count != moves.Count);
    }

    void CheckLevelComplete()
    {
      if(EventSystem.current == null)
        return;

      if(haveDragBox)
        return;

      foreach(var box in boxes)
      {
        if(!box.gameObject.activeSelf)
          continue;

        var position = box.GetSnapPosition();
        if(Mathf.RoundToInt(position.x) == width && Mathf.RoundToInt(position.z) == exitPosition)
        {
          onComplete?.Invoke();
          break;
        }
      }
    }

    public void ShowAll()
    {
      foreach(var box in boxes)
      {
        if(!box.gameObject.activeSelf)
          continue;

        box.ChangeScaleY(1.0f);
      }

      haveDragBox = false;
    }

    [System.Serializable]
    public struct Move
    {
      public Box box;
      public Vector3 start;
      public Vector3 finish;
    }

    List<Move> moves = new List<Move>();
    Move move = new Move();
    int current_move_count;

    public void StartMove(Box target)
    {
      move.start = target.GetSnapPosition();
      move.box = target;

      if(current_move_count != moves.Count)
        moves.RemoveRange(current_move_count, moves.Count - current_move_count);
    }

    public void FinishMove(Box target)
    {
      if(move.start == target.GetSnapPosition())
        return;

      move.finish = target.GetSnapPosition();
      moves.Add(move);
      current_move_count = moves.Count;

      Game.self.Save();
    }

    IEnumerator PlayMove(Box box, Vector3 target)
    {
      var es = EventSystem.current;
      es.enabled = false;

      Vector3 start = box.GetSnapPosition();

      yield return Tween.DoTween(0.1f, (t) => {
        box.transform.position = Vector3.Lerp(start, target, playMoveCurve.Evaluate(t));
      });

      es.enabled = true;

      Game.self.Save();
    }

    public void Undo()
    {
      if(current_move_count == 0)
        return;

      var index = current_move_count - 1;
      StartCoroutine(PlayMove(moves[index].box, moves[index].start));
      current_move_count--;
    }

    public void Redo()
    {
      if(current_move_count == moves.Count)
        return;

      int index = current_move_count;
      StartCoroutine(PlayMove(moves[index].box, moves[index].finish));
      current_move_count++;
    }
  }
}
