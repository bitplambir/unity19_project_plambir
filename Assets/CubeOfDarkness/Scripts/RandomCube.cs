using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CubeOfDarkness
{
  public class RandomCube : MonoBehaviour
  {
    public float minTime;
    public float maxTime;

    public float waitMinTime;
    public float waitMaxTime;

    public float transitionTime;

    IEnumerator setRandomState;

    bool transition;

    public AudioClip transitionClip;
    AudioSource effect;

    void Start()
    {
      transform.localScale = Vector3.zero;
      setRandomState = SetRandomState();
      StartCoroutine(setRandomState);
    }

    void Update()
    {
      if(transition)
        return;

      if(Input.GetMouseButton(0))
      {
        StopCoroutine(setRandomState);
        StartCoroutine(Transition());
      }
    }

    IEnumerator Animation(Vector3 scale, Quaternion rotation, float anim_time, AnimationCurve curve)
    {
      float time = 0.0f;

      var originScale = transform.localScale;
      var originRotation = transform.rotation;

      while(time <= anim_time)
      {
        var t = curve.Evaluate( Mathf.Clamp01(time / anim_time));

        transform.rotation = Quaternion.Slerp(originRotation, rotation, t);
        transform.localScale = Vector3.Lerp(originScale, scale, t);

        time += Time.deltaTime;
        yield return null;
      }
    }

    IEnumerator SoundFade()
    {
      var source = GetComponent<AudioSource>();
      var origin_volume = source.volume;
      var time = 0.0f;

      while(time <= transitionTime)
      {
        source.volume = Mathf.Lerp(origin_volume, 0.0f, Mathf.Clamp01(time / transitionTime));
        time += Time.deltaTime;
        yield return null;
      }
    }

    IEnumerator Transition()
    {
      transition = true;
      AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

      var rotation = Quaternion.identity;

      StartCoroutine(SoundFade());

      yield return Animation(transform.localScale, rotation, transitionTime * 0.5f, curve);

      effect = gameObject.AddComponent<AudioSource>();
      effect.clip = transitionClip;
      effect.Play();

      var scale = Vector3.one;
      scale.x = 50;
      scale.y = 50;
      scale.z = 1;

      yield return Animation(scale, rotation, transitionTime * 0.5f, curve);

      while(effect.isPlaying)
        yield return null;

      SceneManager.LoadScene("GameDark");
    }

    IEnumerator SetRandomState()
    {
      var scale = Vector3.one;
      var rotation = Quaternion.identity;

      AnimationCurve curve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

      yield return new WaitForSeconds(1.5f);

      while(true)
      {
        scale.x = Random.Range(0.5f, 2.0f);
        scale.y = scale.x;
        scale.z = scale.x;

        rotation.x = Random.Range(0.0f, 1.0f);
        rotation.y = Random.Range(0.0f, 1.0f);
        rotation.z = Random.Range(0.0f, 1.0f);
        rotation.w = Random.Range(0.0f, 1.0f);

        yield return Animation(scale, rotation, Random.Range(minTime, maxTime), curve);
        yield return new WaitForSeconds(Random.Range(waitMinTime, waitMaxTime));
      }
    }
  }
}
