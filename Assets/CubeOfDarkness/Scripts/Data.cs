using UnityEngine;
using System.Collections.Generic;

namespace CubeOfDarkness
{
  public class Data : MonoBehaviour
  {
    [System.Serializable]
    public class DataBox
    {
      public int x;
      public int y;
      public Color color;
      public bool vertical;
      public uint size;
    }

    [System.Serializable]
    public class DataLevel
    {
      public uint width;
      public uint height;
      public uint exitPosition;

      public List<DataBox> boxes = new List<DataBox>();
      public List<Level.Move> moves = new List<Level.Move>();
      public int current_move_count;
    }

    public List<DataLevel> levels;

    public Level preview;
  }
}
