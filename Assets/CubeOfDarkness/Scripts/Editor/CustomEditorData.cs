using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace CubeOfDarkness
{
  [CustomEditor(typeof(Data))]
  public class CustomEditorData : Editor
  {
    enum Mode
    {
      MODERN,
      DEFAULT
    };
    Mode mode;

    public override void OnInspectorGUI()
    {
      mode = (Mode)EditorGUILayout.EnumPopup(mode);
      if(mode == Mode.DEFAULT)
        DrawDefaultInspector();
      else
        DrawModernInspector((Data)target);
    }

    public List<T> Swap<T>(ref List<T> list, int index_a, int index_b)
    {
      T tmp = list[index_a];
      list[index_a] = list[index_b];
      list[index_b] = tmp;
      return list;
    }

    void DrawModernInspector(Data data)
    {
      data.preview = (Level)EditorGUILayout.ObjectField("Preview", data.preview, typeof(Level), true);
      for(int i = 0; i < data.levels.Count; ++i)
      {
        var level = data.levels[i];
        GUILayout.BeginVertical(EditorStyles.helpBox);
        GUILayout.BeginHorizontal();

        GUI.enabled = i != 0;
        if(GUILayout.Button("↑", GUILayout.MaxWidth(20)))
        {
          Swap(ref data.levels, i, i - 1);
          GUILayout.EndHorizontal();
          GUILayout.EndVertical();
          break;
        }
        GUI.enabled = i != data.levels.Count - 1;
        if(GUILayout.Button("↓", GUILayout.MaxWidth(20)))
        {
          Swap(ref data.levels, i, i + 1);
          GUILayout.EndHorizontal();
          GUILayout.EndVertical();
          break;
        }
        GUI.enabled = true;

        EditorGUILayout.Space();
        GUILayout.Label($"{i+1}", GUILayout.MaxWidth(50));
        EditorGUILayout.Space();

        if(GUILayout.Button("Load"))
        {
          Load(data, level);
        }
        if(GUILayout.Button("Save"))
        {
          data.levels[i] = data.preview.ToData();
        }
        EditorGUILayout.Space();
        if(GUILayout.Button("X", GUILayout.MaxWidth(20)))
        {
          data.levels.Remove(level);
          GUILayout.EndHorizontal();
          GUILayout.EndVertical();
          break;
        }
        GUILayout.EndHorizontal();

        EditorGUILayout.Space();

        int size = 10;

        var rect = EditorGUILayout.GetControlRect(false, level.height * size);
        GUI.BeginGroup(rect);
        EditorGUI.DrawRect(new Rect(0, 0, level.width * size, level.height * size), Color.white);
        EditorGUI.DrawRect(new Rect(level.width * size, (level.height - level.exitPosition) * size, 2 * size, -size), Color.black);
        foreach(var box in level.boxes)
        {
          if(box.vertical)
            EditorGUI.DrawRect(new Rect(box.x * size, (level.height - box.y) * size, size, -box.size * size), box.color);
          else
            EditorGUI.DrawRect(new Rect(box.x * size, (level.height - box.y) * size, box.size * size, -size), box.color);
        }
        GUI.EndGroup();

        GUILayout.EndVertical();
      }

      if(GUILayout.Button("Save"))
      {
        data.levels.Add(new Data.DataLevel());
        data.levels[data.levels.Count - 1] = data.preview.ToData();
      }
    }

    void Load(Data data, Data.DataLevel level)
    {
      data.preview.Init(level);
    }
  }
}
