using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace CubeOfDarkness
{
  [CustomEditor(typeof(Level))]
  public class CustomEditorLevel : Editor
  {
    public override void OnInspectorGUI()
    {
      DrawDefaultInspector();

      if(GUILayout.Button("Refresh"))
      {
        var level = ((Level)target);
        for(int i = 0; i < level.transform.childCount; ++i)
        {
          var go = level.transform.GetChild(i).gameObject;
          DestroyImmediate(go);
        }
        level.Start();
      }
    }
  }
}
