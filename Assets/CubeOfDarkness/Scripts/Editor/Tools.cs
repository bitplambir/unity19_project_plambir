using UnityEngine;
using UnityEditor;

namespace CubeOfDarkness
{

  static public class HotKeys
  {
    [MenuItem("Game/Tools/Rotate Box %#r")]
    public static void RotateBox()
    {
      if(Selection.objects.Length == 0)
        return;

      foreach(var obj in Selection.objects)
      {

        var go = obj as GameObject;
        if(go == null)
          continue;

        Undo.RecordObject(go.transform, "Rotate To");
        if(go.transform.rotation.eulerAngles.y != 0.0f)
          go.transform.right = new Vector3(1, 0, 0);
        else
          go.transform.right = new Vector3(0, 0, 1);
      }
    }

    [MenuItem("Game/Tools/Select Data %#d")]
    public static void SelectData()
    {
      var obj = GameObject.FindObjectOfType<Data>();
      Selection.objects = new UnityEngine.Object[]{obj.gameObject};
    }

    [MenuItem("Game/Tools/Reset")]
    public static void Reset()
    {
      PlayerPrefs.DeleteAll();
    }
  }

}
