using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CubeOfDarkness {

  public static class Tween
  {
    public static IEnumerator DoTween(float full_time, Action<float> update)
    {
      float time = 0;

      while(time < full_time)
      {
        update(time / full_time);
        time += Time.deltaTime;
        yield return null;
      }

      update(1.0f);
    }
  }
}
