using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Serialization;

namespace CubeOfDarkness
{
  public class Game : MonoBehaviour
  {
    public Data data;

    public Level level;

    public static Game self;

    int current_level_index = 0;
    int max_level_index = 0;
    int preview_level_index = 0;

    public Camera levelCamera;
    public PhysicsRaycaster raycaster;

    public Button prev;
    public Button next;

    public Button play;

    public Text levelCounter;
    public Text complete;
    public Text nextLabel;
    public Text playLabel;

    public Animator levelSelect;
    public bool isLevelSelect;

    float game_camera_size;
    public AnimationCurve cameraCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

    const float LEVEL_PREVIEW_CAMERA_SIZE = 8;

    public void LevelSelect()
    {
      Save();
      preview_level_index = current_level_index;
      level.is_preview_mode = true;
      level.undo.gameObject.SetActive(false);
      level.redo.gameObject.SetActive(false);
      level.levels.gameObject.SetActive(false);
      raycaster.enabled = false;
      StartCoroutine(LevelSelectMode());
    }

    IEnumerator LevelSelectMode()
    {
      in_transition = true;

      UpdateLevelSelect();
      levelSelect.SetTrigger("show");
      yield return Tween.DoTween(0.2f, (t) => {
        levelCamera.orthographicSize = Mathf.Lerp(game_camera_size, LEVEL_PREVIEW_CAMERA_SIZE, cameraCurve.Evaluate(t));
      });
      isLevelSelect = true;

      in_transition = false;
    }

    void UpdateLevelSelect()
    {
      lastChangeLevelTime = Time.time;
      complete.gameObject.SetActive(current_level_index < max_level_index);
      levelCounter.text = $"{current_level_index + 1} / {data.levels.Count}";
      if(current_level_index == data.levels.Count - 1)
      {
        nextLabel.text = "END";
        next.interactable = false;
      }
      else if(current_level_index == max_level_index)
      {
        nextLabel.text = "LOCK";
        next.interactable = false;
      }
      else
      {
        nextLabel.text = ">";
        next.interactable = true;
      }

      prev.interactable = current_level_index != 0;

      if(PlayerPrefs.GetInt("current_level_index", 0) == current_level_index && !string.IsNullOrEmpty(PlayerPrefs.GetString("level", "")))
        playLabel.text = "CONTINUE";
      else
        playLabel.text = "PLAY";
    }

    void Start()
    {
      level.onComplete = OnComplete;

      game_camera_size = levelCamera.orthographicSize;

      next.onClick.AddListener(OnNext);
      prev.onClick.AddListener(OnPrev);
      play.onClick.AddListener(OnPlay);

      isLevelSelect = false;
    }

    float lastChangeLevelTime;

    const float PREVIEW_UPDATE_TIME = 0.25f;

    void UpdateLevelPreview()
    {
      if(!isLevelSelect)
        return;

      if(current_level_index == preview_level_index)
        return;

      if(have_update_preview > 0)
        return;

      if((Time.time - lastChangeLevelTime) > PREVIEW_UPDATE_TIME)
      {
        StartCoroutine(DoUpdateLevelPreview());
        preview_level_index = current_level_index;
      }
    }

    int have_update_preview = 0;

    IEnumerator DoUpdateLevelPreview()
    {
      have_update_preview++;

      level.HideAll();
      yield return new WaitForSeconds(Box.FULL_CHANGE_SCALE_SPEED);

      if(PlayerPrefs.GetInt("current_level_index", 0) == current_level_index)
      {
        var saved_level = PlayerPrefs.GetString("level", "");
        if(string.IsNullOrEmpty(saved_level))
          level.Init(data.levels[current_level_index], hide: true);
        else
          level.Init(JsonUtility.FromJson<Data.DataLevel>(saved_level));
      }
      else
        level.Init(data.levels[current_level_index], hide: true);

      yield return new WaitForSeconds(Box.FULL_CHANGE_SCALE_SPEED);

      level.ShowAll();
      yield return new WaitForSeconds(Box.FULL_CHANGE_SCALE_SPEED);

      have_update_preview--;
    }

    void OnNext()
    {
      if(current_level_index >= max_level_index)
        return;

      current_level_index++;

      UpdateLevelSelect();
    }

    void OnPrev()
    {
      if(current_level_index == 0)
        return;

      current_level_index--;

      UpdateLevelSelect();
    }

    bool in_transition;

    IEnumerator DoPlay()
    {
      in_transition = true;

      levelSelect.SetTrigger("hide");
      isLevelSelect = false;

      while(have_update_preview != 0)
        yield return null;

      if(preview_level_index != current_level_index)
        yield return DoUpdateLevelPreview();

      yield return Tween.DoTween(0.2f, (t) => {
        levelCamera.orthographicSize = Mathf.Lerp(LEVEL_PREVIEW_CAMERA_SIZE, game_camera_size, cameraCurve.Evaluate(t));
      });
      level.is_preview_mode = false;
      raycaster.enabled = true;
      level.levels.gameObject.SetActive(true);
      Save();

      in_transition = false;
    }

    void OnPlay()
    {
      StartCoroutine(DoPlay());
    }

    void OnComplete()
    {
      StartCoroutine(ChangeLevel());
    }

    void NewLevel()
    {
      if(++current_level_index >= data.levels.Count)
        current_level_index = 0;
      max_level_index = Math.Max(current_level_index, max_level_index);
      level.Init(data.levels[current_level_index], hide: true);
    }

    IEnumerator ChangeLevel()
    {
      var es = EventSystem.current;
      es.enabled = false;

      yield return new WaitForSeconds(Box.FULL_CHANGE_SCALE_SPEED);

      level.HideAll();
      yield return new WaitForSeconds(Box.FULL_CHANGE_SCALE_SPEED);
      NewLevel();
      yield return new WaitForSeconds(Box.FULL_CHANGE_SCALE_SPEED);

      level.ShowAll();
      yield return new WaitForSeconds(Box.FULL_CHANGE_SCALE_SPEED);

      es.enabled = true;

      Save();
    }

    void Awake()
    {
      Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);

      if(self == null)
        self = this;

      Input.multiTouchEnabled = false;

      Load();
    }

    void Update()
    {
      UpdateLevelPreview();

      if(Input.GetKeyUp(KeyCode.Space))
      {
        if(Time.timeScale == 1.0f)
          Time.timeScale = 0.1f;
        else
          Time.timeScale = 1.0f;
      }

      if(Input.GetKeyUp(KeyCode.Escape) && !in_transition)
      {
        if(isLevelSelect)
        {
          current_level_index = PlayerPrefs.GetInt("current_level_index", 0);
          OnPlay();
        }
        else
          Application.Quit();
      }
    }

    void Load()
    {
      current_level_index = PlayerPrefs.GetInt("current_level_index", 0);
      max_level_index = PlayerPrefs.GetInt("max_level_index", 0);
      var saved_level = PlayerPrefs.GetString("level", "");
      if(string.IsNullOrEmpty(saved_level))
        level.Init(data.levels[current_level_index]);
      else
        level.Init(JsonUtility.FromJson<Data.DataLevel>(saved_level));
    }

    public void Save()
    {
      PlayerPrefs.SetInt("current_level_index", current_level_index);
      PlayerPrefs.SetInt("max_level_index", max_level_index);
      PlayerPrefs.SetString("level", JsonUtility.ToJson(level.ToData()));
    }

    void OnApplicationQuit()
    {
      if(isLevelSelect)
        return;

      Save();
    }
  }
}
