﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Base;

namespace Game1
{

[RequireComponent(typeof(CoroutineAnimation))]
[RequireComponent(typeof(Rigidbody))]
public class Unit : MonoBehaviour
{
  public float speed = 1.0f;
  public Vector3 move_vector;

  public Action on_coin;

  public void Move(Vector3 move_vector)
  {
    this.move_vector = move_vector;
  }

  void FixedUpdate()
  {
    var dt = Time.fixedDeltaTime;

    transform.position += move_vector * speed * dt;

    if(transform.position.y < -0.5f)
      Die();
  }

  CoroutineAnimation coroutine_animation;

  void Start()
  {
    coroutine_animation = GetComponent<CoroutineAnimation>();
  }

  void Die()
  {
    if(gameObject.tag != "Player")
      return;

    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  void OnTriggerEnter(Collider other)
  {
    if(other.gameObject.tag != "Player")
      return;

    if(gameObject.tag == "Coin")
    {
      if(coroutine_animation.IsBusy)
        return;

      on_coin?.Invoke();
      coroutine_animation.StartAnimation(CoroutineAnimation.Animation.DIE, 0.2f, () => {
        GameObject.Destroy(gameObject);
      });
    }
    else
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }
}

}
