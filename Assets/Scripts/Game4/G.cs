using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game4
{

public class G : MonoBehaviour
{
  public static G self;

  public Text victory;

  void Awake()
  {
    self = this;
  }

  void Start()
  {
    victory.gameObject.SetActive(false);
  }

  public void OnVictory(bool is_victory)
  {
    if(is_victory)
    {
      victory.gameObject.SetActive(true);
      StartCoroutine(WaitAndReload());
    }
    else
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  IEnumerator WaitAndReload()
  {
    yield return new WaitForSeconds(1.0f);
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  void Update()
  {
  }
}

}
