using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game4
{

public class VirtualInput : MonoBehaviour
{
  public float horizontal
  {
    get {
      return velocity.x;
    }
    set {
      velocity.x = value;
    }
  }

  public float vertical
  {
    get {
      return velocity.z;
    }
    set {
      velocity.z = value;
    }
  }

  Vector3 velocity;

  public bool pressA;

  Vector3 buttonPosition {
    get {
      return buttonA.transform.position;
    }
  }
  Vector3 joystickPosition {
    get {
      return stick.transform.position;
    }
  }
  Vector2 touchDist;

  int buttonAFingerId = -1;
  int joystickFingerId = -1;
  float joystickMaxDistance {
    get {
      return stick.GetComponent<RectTransform>().rect.width / 2.0f - head.GetComponent<RectTransform>().rect.width / 2;
    }
  }

  public RawImage stick;
  public RawImage head;
  public RawImage buttonA;

  Rect buttonARect {
    get {
      return buttonA.GetComponent<RectTransform>().rect;
    }
  }

  bool updateJoystick;
  bool updateButtonA;

  void SetVirtualJoystickActive(bool state)
  {
    stick.gameObject.SetActive(state);
    head.gameObject.SetActive(state);
    buttonA.gameObject.SetActive(state);
  }

  Dictionary<int, bool> fingers = new Dictionary<int, bool>();
  List<int> finger_ids = new List<int>();

  void UpdateUnityInput()
  {
    horizontal = Input.GetAxis("Horizontal");
    vertical =  Input.GetAxis("Vertical");
    pressA = Input.GetKey(KeyCode.Space);
  }

  void UpdateVirtualInput()
  {
    foreach(var key in finger_ids)
      fingers[key] = false;

    for(int i = 0; i < Input.touchCount; ++i)
    {
      SetVirtualJoystickActive(true);
      var touch = Input.GetTouch(i);

      if(fingers.ContainsKey(touch.fingerId))
        finger_ids.Add(touch.fingerId);
      fingers[touch.fingerId] = true;

      var pressed = touch.phase == TouchPhase.Began ||
        touch.phase == TouchPhase.Moved ||
        touch.phase == TouchPhase.Stationary;
      var position = touch.position;
      UpdateJoystick(pressed, touch.position, touch.fingerId);
      UpdateButtonA(pressed, touch.position, touch.fingerId);
    }

    foreach(var finger in fingers)
    {
      if(!finger.Value)
      {
        UpdateJoystick(false, Vector3.zero, finger.Key);
        UpdateButtonA(false, Vector3.zero, finger.Key);
      }
    }

    updateJoystick = false;
    updateButtonA = false;
  }

  public void Tick()
  {
    if(Input.anyKey)
      SetVirtualJoystickActive(false);

    UpdateUnityInput();

    UpdateVirtualInput();
  }

  bool IsTouch(bool pressed, Vector3 touchPosition, int fingerId, Vector3 controllPosition, int controllFingerId, float safeTouchDistance)
  {
    var touchDistance = (touchPosition - controllPosition).magnitude;

    if(controllFingerId >= 0 && fingerId != controllFingerId)
      return false;

    if(controllFingerId < 0 && touchDistance > safeTouchDistance)
      return false;

    return true;
  }

  void UpdateButtonA(bool pressed, Vector3 touchPosition, int fingerId)
  {
    if(updateButtonA)
      return;

    if(!IsTouch(pressed, touchPosition, fingerId, buttonPosition, buttonAFingerId, buttonARect.width / 2.0f))
      return;

    buttonAFingerId = fingerId;

    updateButtonA = true;

    pressA = pressed;
    if(!pressA)
      buttonAFingerId = -1;

    buttonA.transform.localScale = Vector3.one * (pressA ? 0.8f : 1.0f);
  }

  void UpdateJoystick(bool pressed, Vector3 touchPosition, int fingerId)
  {
    if(updateJoystick)
      return;

    if(!IsTouch(pressed, touchPosition, fingerId, joystickPosition, joystickFingerId, joystickMaxDistance * 1.5f))
      return;

    joystickFingerId = fingerId;

    updateJoystick = true;

    if(pressed)
    {
      touchDist = touchPosition - joystickPosition;

      var distance = Mathf.Min(touchDist.magnitude, joystickMaxDistance);
      touchDist = touchDist.normalized * distance;
      touchDist.x = touchDist.x / joystickMaxDistance;
      touchDist.y = touchDist.y / joystickMaxDistance;

      horizontal = touchDist.x;
      vertical = touchDist.y;
    }
    else
    {
      touchDist = Vector2.zero;
      joystickFingerId = -1;
    }

    head.transform.localPosition = new Vector3(horizontal * joystickMaxDistance, vertical * joystickMaxDistance, 0.0f);
  }

  public Vector3 GetVelocity()
  {
    return velocity;
  }
}

}
