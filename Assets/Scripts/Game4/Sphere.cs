using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Game4
{

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(VirtualInput))]
public class Sphere : MonoBehaviour
{
  public float speed;
  public float jumpSpeed;

  public RawImage heart;
  List<RawImage> hearts = new List<RawImage>();

  public int maxLife;
  int life;

  Rigidbody self;
  VirtualInput input;
  new Collider collider;

  bool makeJump;

  public Vector3 velocity;
  public bool is_ground;

  Vector3 prev_pos;

  void Start()
  {
    self = GetComponent<Rigidbody>();
    input = GetComponent<VirtualInput>();
    collider = GetComponent<Collider>();
    prev_pos = transform.position;

    Physics.IgnoreLayerCollision(LayerMask.NameToLayer("player"), LayerMask.NameToLayer("enemy"), ignore: false);

    var canvas = GetComponentInChildren<Canvas>();
    if(canvas != null)
      GetComponentInChildren<Canvas>().GetComponent<RectTransform>().SetParent(null);

    life = maxLife;
    hearts.Add(heart);
    for(int i = 1; i < maxLife; ++i)
      hearts.Add(RawImage.Instantiate(heart, heart.transform.parent));
  }

  void Update()
  {
    for(int i = 0; i < maxLife; ++i)
      hearts[i].gameObject.SetActive(i < life);
  }

  void FixedUpdate()
  {
    input.Tick();

    self.AddForce(input.GetVelocity() * speed * Time.fixedDeltaTime, ForceMode.Acceleration);

    is_ground = IsGround();

    if(input.pressA && !makeJump && is_ground)
    {
      self.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
      makeJump = true;
    }

    if(makeJump)
      makeJump = input.pressA;

    if(transform.position.y < -10.0f)
      G.self.OnVictory(false);

    velocity = self.velocity;

    Debug.DrawLine(transform.position, prev_pos, Color.red, 20.0f);
    prev_pos = transform.position;
  }

  void OnTriggerEnter(Collider other)
  {
    var vc = other.gameObject.GetComponent<VictoryCube>();
    if(vc != null)
    {
      G.self.OnVictory(vc.is_victory);
      self.velocity = Vector3.zero;
      self.angularVelocity = Vector3.zero;
    }
  }

  Vector3 GetGroundPoint(Collider collider)
  {
    var point = collider.bounds.center;
    point.y = collider.bounds.min.y;
    return point;
  }

  Vector3 ground_collider_size = new Vector3(0.2f, 0.1f, 0.2f);

#if UNITY_EDITOR
  void OnDrawGizmosSelected()
  {
    Collider collider = GetComponent<Collider>();
    UnityEditor.Handles.color = Color.green;
    UnityEditor.Handles.DrawWireCube(GetGroundPoint(collider), ground_collider_size);
  }
#endif


  bool IsGround()
  {
    return Physics.CheckBox(GetGroundPoint(collider), ground_collider_size, Quaternion.identity, layerMask: 1 << LayerMask.GetMask("player"));
  }

  public void Hit()
  {
    --life;
    if(life <= 0)
      G.self.OnVictory(false);

    Physics.IgnoreLayerCollision(LayerMask.NameToLayer("player"), LayerMask.NameToLayer("enemy"), ignore: true);
    StartCoroutine(GodMode());
  }

  IEnumerator GodMode()
  {
    yield return new WaitForSeconds(1.0f);
    Physics.IgnoreLayerCollision(LayerMask.NameToLayer("player"), LayerMask.NameToLayer("enemy"), ignore: false);
  }
}

}
