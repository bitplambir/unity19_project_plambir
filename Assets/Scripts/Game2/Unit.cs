﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game2
{

public class Unit : MonoBehaviour
{
  public GameObject mp_regen;

  [System.Serializable]
  public class Effect
  {
    public float damage;
    public GameObject vfx;
    public int duration_turn;

    public Effect Clone()
    {
      return new Effect(){damage = this.damage, vfx = this.vfx, duration_turn = this.duration_turn};
    }
  }

  [System.Serializable]
  public class Ability
  {
    public enum TargetType
    {
      ENEMY,
      SELF
    }

    public string name;
    public float damage;
    public int mp;
    public TargetType target_type;

    public string animation_trigger;

    public GameObject projectile_parent;
    public Effect effect;
  }

  [System.NonSerialized]
  public float hp;
  public float max_hp;

  [System.NonSerialized]
  public float mp;
  public float max_mp;

  [System.NonSerialized]
  public Unit target;
  public float change_target_speed = 5.0f;

  public List<Ability> abilities;
  List<Effect> effects = new List<Effect>();

  Animator animator;
  bool is_animation_block {
    get {
      return animation_block_counter > 0;
    }
  }
  int animation_block_counter = 0;

  GameObject projectile_parent;

  void Awake()
  {
    animator = GetComponent<Animator>();
    hp = max_hp;
    mp = max_mp;
  }

  IEnumerator Hit(Effect effect)
  {
    yield return HitDamage(effect.damage);
  }

  public IEnumerator Hit(Ability ability)
  {
    yield return HitDamage(ability.damage);

    if(ability.effect.damage > 0 && IsAlive())
    {
      var new_effect = ability.effect.Clone();
      new_effect.vfx = GameObject.Instantiate(ability.effect.vfx, transform);
      effects.Add(new_effect);
    }
  }

  IEnumerator HitDamage(float damage)
  {
    hp = Mathf.Clamp(hp - damage, 0, max_hp) ;

    if(!IsAlive())
      StartAnimation("die");

    yield return new WaitForSeconds(0.2f);
  }

  public IEnumerator UseAbility(Ability ability)
  {
    projectile_parent = ability.projectile_parent;
    mp = Mathf.Clamp(mp - ability.mp, 0, max_mp);
    if(!string.IsNullOrEmpty(ability.animation_trigger))
    {
      StartAnimation(ability.animation_trigger);
      while(is_animation_block)
        yield return null;
    }
  }

  public void SpawnProjectile(Projectile origin)
  {
    animation_block_counter++;
    var projectile = Projectile.Instantiate(origin);
    projectile.transform.position = projectile_parent != null ? projectile_parent.transform.position : transform.position;
    projectile.target = target;
    projectile.on_die = AnimationEnd;
  }

  public IEnumerator OnTurnStart()
  {
    if(mp_regen != null && mp < max_mp)
      SpawnVFX(mp_regen);

    mp = Mathf.Min(mp + 1, max_mp);

    for(int i = effects.Count; i-- > 0;)
    {
      var effect = effects[i];
      effect.duration_turn--;
      yield return Hit(effect);

      if(effect.duration_turn <= 0)
      {
        GameObject.Destroy(effect.vfx);
        effects.RemoveAt(i);
      }
    }
  }

  public void LookAtTarget(Unit other)
  {
    target = other;
  }

  void Update()
  {
    if(!IsAlive())
      return;

    var target_forward = (target.transform.position - transform.position).normalized;
    var angle = Vector3.Angle(target_forward, transform.forward);
    transform.forward = Vector3.Slerp(transform.forward, target_forward, change_target_speed / angle);
  }

  public bool IsAlive()
  {
    return hp > 0;
  }

  public void SpawnVFX(GameObject vfx)
  {
    GameObject.Instantiate(vfx, transform);
  }

  public void StartAnimation(string trigger_name)
  {
    animation_block_counter++;
    animator.SetTrigger(trigger_name);
  }

  public void PlayAudioClip(AudioClip clip)
  {
    var audio_source = GetComponent<AudioSource>();
    if(audio_source == null)
    {
      audio_source = gameObject.AddComponent<AudioSource>();
      audio_source.loop = false;
      audio_source.playOnAwake = false;
    }

    audio_source.clip = clip;
    audio_source.Play();
  }

  public void AnimationEnd()
  {
    animation_block_counter--;
  }
}

}
